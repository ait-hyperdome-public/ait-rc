import React from 'react';
import { AgGridReactProps } from 'ag-grid-react';
export type * from 'ag-grid-react';
export type * from 'ag-grid-community';
export declare const Table: React.MemoExoticComponent<(props: AgGridReactProps) => import("react/jsx-runtime").JSX.Element>;
