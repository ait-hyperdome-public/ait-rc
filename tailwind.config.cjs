/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js,jsx}', './lib/**/*.{html,js,jsx}'],
  theme: {
    extend: {
      boxShadow: {
        default: 'rgba(14, 30, 37, 0.12) 0px 2px 4px 0px, rgba(14, 30, 37, 0.32) 0px 2px 16px 0px;'
      }
    }
  },
  plugins: [],
  corePlugins: {
    preflight: true
  },
  important: true
}
