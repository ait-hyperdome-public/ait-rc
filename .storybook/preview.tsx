import type { Preview } from "@storybook/react";

import React from "react";
import { ConfigProvider, defaultTheme } from '../lib'
import 'ag-grid-community/styles/ag-grid.css'
import 'ag-grid-community/styles/ag-theme-quartz.css'
import 'ag-grid-community/styles/agGridQuartzFont.css'

const App = function(Story) {
  return (
    <ConfigProvider theme={defaultTheme} >
      <Story />
    </ConfigProvider>
  )
}

const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
  },
  decorators: [App]
};

export default preview;
