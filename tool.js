import fs from 'fs'
import path from 'path'

const files = fs.readdirSync(path.join(process.cwd(), 'lib', 'assets', 'svg'))
function genImport(filename, name, dir = '../assets/svg') {
  return `import ${name}SVG from '${dir}/${filename}?react'`
}
function genExport(name) {
  return `export const ${name} = (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={${name}SVG} {...props} />`
}
files.forEach(filename => {
  const [_, name] = filename.split('_')
  const iconName = name.slice(0, name.length - path.extname(name).length)
  // console.log(genImport(filename, iconName))
  // console.log(genExport(iconName))
})
console.log(files.map(filename => {
  const [_, name] = filename.split('_')
  const iconName = name.slice(0, name.length - path.extname(name).length)
  return `"${iconName}"`
}).join('|'))