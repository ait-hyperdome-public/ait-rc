import React from 'react';
import Icon from '@ant-design/icons';
interface IconBaseProps extends React.HTMLProps<HTMLSpanElement> {
    spin?: boolean;
    rotate?: number;
}
interface CustomIconComponentProps {
    width: string | number;
    height: string | number;
    fill: string;
    viewBox?: string;
    className?: string;
    style?: React.CSSProperties;
}
interface IconComponentProps extends IconBaseProps {
    viewBox?: string;
    component?: React.ComponentType<CustomIconComponentProps | React.SVGProps<SVGSVGElement>> | React.ForwardRefExoticComponent<CustomIconComponentProps>;
    ariaLabel?: React.AriaAttributes['aria-label'];
}
export * from '@ant-design/icons';
export type AITIconType = (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => JSX.Element;
export type IconName = "Add" | "AddCircled" | "Arrow1Down" | "Arrow1Left" | "Arrow1LeftDown" | "Arrow1LeftUp" | "Arrow1Right" | "Arrow1RightDown" | "Arrow1RightUp" | "Arrow1Up" | "Arrow2Down" | "Arrow2Left" | "Arrow2LeftDown" | "Arrow2LeftUp" | "Arrow2Right" | "Arrow2RightDown" | "Arrow2RightUp" | "Arrow2Up" | "Arrow3Down" | "Arrow3Left" | "Arrow3Right" | "Arrow3Up" | "Arrow4Down" | "Arrow4Left" | "Arrow4LeftDown" | "Arrow4LeftUp" | "Arrow4Right" | "Arrow4RightDown" | "Arrow4RightUp" | "Arrow4Up" | "Arrow5DownLeft" | "Arrow5DownRight" | "Arrow5LeftDown" | "Arrow5LeftUp" | "Arrow5RightDown" | "Arrow5RightUp" | "Arrow5UpLeft" | "Arrow5UpRight" | "Attach" | "Bell" | "Book" | "Bookmark" | "Calendar" | "Check" | "CheckCircled" | "Clear" | "Clock" | "Comment" | "Cookie" | "Copy" | "Cut" | "Database" | "Delete" | "DeleteCircled" | "Edit" | "EnvelopeClose" | "EnvelopeOpen" | "Favorite" | "Filter" | "Find" | "Flag" | "FolderClose" | "FolderOpen" | "Forbid" | "FullScreen" | "FullScreenExit" | "Home" | "Hyperlink" | "Image" | "Info" | "Label" | "List" | "New" | "Next" | "Options" | "Previous" | "Print" | "Question" | "Rating" | "Redo" | "Refresh" | "Reload" | "Remove" | "RemoveCircled" | "Rollback" | "Send" | "Settings" | "Sum" | "Table" | "Trash" | "Undo" | "User" | "Window" | "Zoom" | "Bank" | "BarChart" | "Briefcase" | "Businessman" | "Businesswoman" | "Calculator" | "Cash" | "CreditCard" | "Diagram" | "Dollar" | "DollarCircled" | "DoughnutChart" | "Euro" | "EuroCircled" | "Idea" | "LinearChart" | "Money" | "Phone" | "PieChart" | "Presentation" | "Report" | "Safe" | "Target" | "World" | "DesktopMac" | "DesktopWindows" | "Headphone" | "Keyboard" | "LaptopMac" | "LaptopWindows" | "Microphone" | "Mouse" | "PhoneAndroid" | "PhoneIphone" | "Photo" | "Printer" | "Router" | "Scanner" | "TabletMac" | "TabletWindows" | "TV" | "Video" | "Volume" | "Assistance" | "Bug" | "Fingerprint" | "Key" | "Lock" | "PersonalID" | "Security" | "Stop" | "Unlock" | "Visibility" | "VisibilityOff" | "Warning" | "WarningCircled1" | "WarningCircled2" | "Barcode" | "Box" | "CashVoucher" | "Coupon" | "Delivery" | "Favorites" | "Gift" | "New2" | "Package" | "Percent" | "Promotion" | "Sales" | "ShoppingBasket" | "ShoppingCart" | "Store" | "Wallet" | "Anchor" | "Bar" | "Beach" | "Bus" | "Cafe" | "Camping" | "Car" | "CurrencyExchange" | "DoorHanger" | "Forest" | "Handwheel" | "Hotel" | "Map" | "MapPointer" | "Mountains" | "Passport" | "Plane" | "Pub" | "ReceptionBell" | "Rest" | "Restaurant" | "Ship" | "Suitcase" | "Swim" | "Taxi" | "Train" | "Walk" | "Cloudy" | "DegreeCelsius" | "DegreeFahrenheit" | "Fog" | "Hail" | "Humidity" | "Lightning" | "Moon" | "PartlyCloudyDay" | "PartlyCloudyNight" | "Rain" | "RainAndHail" | "RainHeavy" | "RainLight" | "Snow" | "Snowfall" | "SnowfallHeavy" | "SnowfallLight" | "Storm" | "Sunny" | "Temperature" | "Umbrella" | "Water" | "Wind" | "WindDirection";
export declare const AITIcon: {
    [key in IconName]: AITIconType;
};
export { Icon };
