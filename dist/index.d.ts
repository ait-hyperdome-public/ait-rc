import type { ThemeConfig } from 'antd';
export * from './components';
export * from './icons';
export declare const defaultTheme: ThemeConfig;
