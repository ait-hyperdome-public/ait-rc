import { Meta, StoryObj } from '@storybook/react'

import { Button } from '../../lib'

const meta = {
  title: 'Lib/Button',
  component: Button,
} satisfies Meta<typeof Button>

export default meta
type Story = StoryObj<typeof Button>

export const Primary: Story = {
  args: {
    children: 'Primary Button',
    type: 'primary'
  }
}
