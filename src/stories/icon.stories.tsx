import { Meta, StoryObj } from '@storybook/react'
import { AITIcon, type AITIconType, Col, Row, type IconName } from '../../lib'
import React from 'react'

const center: React.CSSProperties = {
  display: 'flex',
  flexDirection: 'column',
  justifyItems: 'center',
  alignItems: 'center',
  marginTop: 24
}
const iconStype: React.CSSProperties = {
  fontSize: 48
}
function IconItem (props: { name: string, Component: AITIconType }) {
  const { name, Component } = props
  return (
    <Col style={center} span={2}>
      <Component style={iconStype} />
      <div>{name}</div>
    </Col>
  )
}
function Icons () {
  return (
    <Row gutter={16}>
      {Object.keys(AITIcon).map((key) => <IconItem key={key} name={key} Component={AITIcon[key as IconName]} />)}
    </Row>
  )
}

const meta = {
  title: 'Lib/Icons',
  component: Icons
} satisfies Meta<typeof Icons>

export default meta

type Story = StoryObj<typeof Icons>

export const AIT_Icons: Story = {
  args: {}
}