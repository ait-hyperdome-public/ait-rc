import type { ThemeConfig } from 'antd'
import 'ag-grid-community/styles/ag-grid.css'
import 'ag-grid-community/styles/ag-theme-quartz.css'
import 'ag-grid-community/styles/agGridQuartzFont.css'
import './style.css'

export * from './components'
export * from './icons'

export const defaultTheme: ThemeConfig = {
  token: {
    colorPrimary: '#00b96b',
    fontFamily: 'SF Pro',
    fontSize: 14
  },
  components: {
    Layout: {
      // layoutHeaderHeight: 45,
      // layoutHeaderPaddingInline: 0,
      // layoutFooterPadding: 0,
      colorBgHeader: '#fafafa'
    }
  }
}