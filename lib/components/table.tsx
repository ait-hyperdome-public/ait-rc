import React from 'react'
import { AgGridReact, AgGridReactProps } from 'ag-grid-react'
import { theme } from 'antd'

export type * from 'ag-grid-react'
export type * from 'ag-grid-community'

const { useToken } = theme

export const Table = React.memo((props: AgGridReactProps) => {
  const { token } = useToken()
  const customStyle = React.useMemo(() => ({
    width: '100%',
    height: '100%',
    '--ag-checkbox-checked-color': token.colorPrimary,
    '--ag-input-focus-box-shadow': `${token.colorPrimary}cc 0px 0px 0px 0px`
  }), [token])

  return (
    <div style={customStyle}>
      <AgGridReact {...props} />
    </div>
  )
})
