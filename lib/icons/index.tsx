import React from 'react'
import Icon from '@ant-design/icons'

import AddSVG from '../assets/svg/Actions_Add.svg?react'
import AddCircledSVG from '../assets/svg/Actions_AddCircled.svg?react'
import Arrow1DownSVG from '../assets/svg/Actions_Arrow1Down.svg?react'
import Arrow1LeftSVG from '../assets/svg/Actions_Arrow1Left.svg?react'
import Arrow1LeftDownSVG from '../assets/svg/Actions_Arrow1LeftDown.svg?react'
import Arrow1LeftUpSVG from '../assets/svg/Actions_Arrow1LeftUp.svg?react'
import Arrow1RightSVG from '../assets/svg/Actions_Arrow1Right.svg?react'
import Arrow1RightDownSVG from '../assets/svg/Actions_Arrow1RightDown.svg?react'
import Arrow1RightUpSVG from '../assets/svg/Actions_Arrow1RightUp.svg?react'
import Arrow1UpSVG from '../assets/svg/Actions_Arrow1Up.svg?react'
import Arrow2DownSVG from '../assets/svg/Actions_Arrow2Down.svg?react'
import Arrow2LeftSVG from '../assets/svg/Actions_Arrow2Left.svg?react'
import Arrow2LeftDownSVG from '../assets/svg/Actions_Arrow2LeftDown.svg?react'
import Arrow2LeftUpSVG from '../assets/svg/Actions_Arrow2LeftUp.svg?react'
import Arrow2RightSVG from '../assets/svg/Actions_Arrow2Right.svg?react'
import Arrow2RightDownSVG from '../assets/svg/Actions_Arrow2RightDown.svg?react'
import Arrow2RightUpSVG from '../assets/svg/Actions_Arrow2RightUp.svg?react'
import Arrow2UpSVG from '../assets/svg/Actions_Arrow2Up.svg?react'
import Arrow3DownSVG from '../assets/svg/Actions_Arrow3Down.svg?react'
import Arrow3LeftSVG from '../assets/svg/Actions_Arrow3Left.svg?react'
import Arrow3RightSVG from '../assets/svg/Actions_Arrow3Right.svg?react'
import Arrow3UpSVG from '../assets/svg/Actions_Arrow3Up.svg?react'
import Arrow4DownSVG from '../assets/svg/Actions_Arrow4Down.svg?react'
import Arrow4LeftSVG from '../assets/svg/Actions_Arrow4Left.svg?react'
import Arrow4LeftDownSVG from '../assets/svg/Actions_Arrow4LeftDown.svg?react'
import Arrow4LeftUpSVG from '../assets/svg/Actions_Arrow4LeftUp.svg?react'
import Arrow4RightSVG from '../assets/svg/Actions_Arrow4Right.svg?react'
import Arrow4RightDownSVG from '../assets/svg/Actions_Arrow4RightDown.svg?react'
import Arrow4RightUpSVG from '../assets/svg/Actions_Arrow4RightUp.svg?react'
import Arrow4UpSVG from '../assets/svg/Actions_Arrow4Up.svg?react'
import Arrow5DownLeftSVG from '../assets/svg/Actions_Arrow5DownLeft.svg?react'
import Arrow5DownRightSVG from '../assets/svg/Actions_Arrow5DownRight.svg?react'
import Arrow5LeftDownSVG from '../assets/svg/Actions_Arrow5LeftDown.svg?react'
import Arrow5LeftUpSVG from '../assets/svg/Actions_Arrow5LeftUp.svg?react'
import Arrow5RightDownSVG from '../assets/svg/Actions_Arrow5RightDown.svg?react'
import Arrow5RightUpSVG from '../assets/svg/Actions_Arrow5RightUp.svg?react'
import Arrow5UpLeftSVG from '../assets/svg/Actions_Arrow5UpLeft.svg?react'
import Arrow5UpRightSVG from '../assets/svg/Actions_Arrow5UpRight.svg?react'
import AttachSVG from '../assets/svg/Actions_Attach.svg?react'
import BellSVG from '../assets/svg/Actions_Bell.svg?react'
import BookSVG from '../assets/svg/Actions_Book.svg?react'
import BookmarkSVG from '../assets/svg/Actions_Bookmark.svg?react'
import CalendarSVG from '../assets/svg/Actions_Calendar.svg?react'
import CheckSVG from '../assets/svg/Actions_Check.svg?react'
import CheckCircledSVG from '../assets/svg/Actions_CheckCircled.svg?react'
import ClearSVG from '../assets/svg/Actions_Clear.svg?react'
import ClockSVG from '../assets/svg/Actions_Clock.svg?react'
import CommentSVG from '../assets/svg/Actions_Comment.svg?react'
import CookieSVG from '../assets/svg/Actions_Cookie.svg?react'
import CopySVG from '../assets/svg/Actions_Copy.svg?react'
import CutSVG from '../assets/svg/Actions_Cut.svg?react'
import DatabaseSVG from '../assets/svg/Actions_Database.svg?react'
import DeleteSVG from '../assets/svg/Actions_Delete.svg?react'
import DeleteCircledSVG from '../assets/svg/Actions_DeleteCircled.svg?react'
import EditSVG from '../assets/svg/Actions_Edit.svg?react'
import EnvelopeCloseSVG from '../assets/svg/Actions_EnvelopeClose.svg?react'
import EnvelopeOpenSVG from '../assets/svg/Actions_EnvelopeOpen.svg?react'
import FavoriteSVG from '../assets/svg/Actions_Favorite.svg?react'
import FilterSVG from '../assets/svg/Actions_Filter.svg?react'
import FindSVG from '../assets/svg/Actions_Find.svg?react'
import FlagSVG from '../assets/svg/Actions_Flag.svg?react'
import FolderCloseSVG from '../assets/svg/Actions_FolderClose.svg?react'
import FolderOpenSVG from '../assets/svg/Actions_FolderOpen.svg?react'
import ForbidSVG from '../assets/svg/Actions_Forbid.svg?react'
import FullScreenSVG from '../assets/svg/Actions_FullScreen.svg?react'
import FullScreenExitSVG from '../assets/svg/Actions_FullScreenExit.svg?react'
import HomeSVG from '../assets/svg/Actions_Home.svg?react'
import HyperlinkSVG from '../assets/svg/Actions_Hyperlink.svg?react'
import ImageSVG from '../assets/svg/Actions_Image.svg?react'
import InfoSVG from '../assets/svg/Actions_Info.svg?react'
import LabelSVG from '../assets/svg/Actions_Label.svg?react'
import ListSVG from '../assets/svg/Actions_List.svg?react'
import NewSVG from '../assets/svg/Actions_New.svg?react'
import NextSVG from '../assets/svg/Actions_Next.svg?react'
import OptionsSVG from '../assets/svg/Actions_Options.svg?react'
import PreviousSVG from '../assets/svg/Actions_Previous.svg?react'
import PrintSVG from '../assets/svg/Actions_Print.svg?react'
import QuestionSVG from '../assets/svg/Actions_Question.svg?react'
import RatingSVG from '../assets/svg/Actions_Rating.svg?react'
import RedoSVG from '../assets/svg/Actions_Redo.svg?react'
import RefreshSVG from '../assets/svg/Actions_Refresh.svg?react'
import ReloadSVG from '../assets/svg/Actions_Reload.svg?react'
import RemoveSVG from '../assets/svg/Actions_Remove.svg?react'
import RemoveCircledSVG from '../assets/svg/Actions_RemoveCircled.svg?react'
import RollbackSVG from '../assets/svg/Actions_Rollback.svg?react'
import SendSVG from '../assets/svg/Actions_Send.svg?react'
import SettingsSVG from '../assets/svg/Actions_Settings.svg?react'
import SumSVG from '../assets/svg/Actions_Sum.svg?react'
import TableSVG from '../assets/svg/Actions_Table.svg?react'
import TrashSVG from '../assets/svg/Actions_Trash.svg?react'
import UndoSVG from '../assets/svg/Actions_Undo.svg?react'
import UserSVG from '../assets/svg/Actions_User.svg?react'
import WindowSVG from '../assets/svg/Actions_Window.svg?react'
import ZoomSVG from '../assets/svg/Actions_Zoom.svg?react'
import BankSVG from '../assets/svg/Business_Bank.svg?react'
import BarChartSVG from '../assets/svg/Business_BarChart.svg?react'
import BriefcaseSVG from '../assets/svg/Business_Briefcase.svg?react'
import BusinessmanSVG from '../assets/svg/Business_Businessman.svg?react'
import BusinesswomanSVG from '../assets/svg/Business_Businesswoman.svg?react'
import CalculatorSVG from '../assets/svg/Business_Calculator.svg?react'
import CashSVG from '../assets/svg/Business_Cash.svg?react'
import CreditCardSVG from '../assets/svg/Business_CreditCard.svg?react'
import DiagramSVG from '../assets/svg/Business_Diagram.svg?react'
import DollarSVG from '../assets/svg/Business_Dollar.svg?react'
import DollarCircledSVG from '../assets/svg/Business_DollarCircled.svg?react'
import DoughnutChartSVG from '../assets/svg/Business_DoughnutChart.svg?react'
import EuroSVG from '../assets/svg/Business_Euro.svg?react'
import EuroCircledSVG from '../assets/svg/Business_EuroCircled.svg?react'
import IdeaSVG from '../assets/svg/Business_Idea.svg?react'
import LinearChartSVG from '../assets/svg/Business_LinearChart.svg?react'
import MoneySVG from '../assets/svg/Business_Money.svg?react'
import PhoneSVG from '../assets/svg/Business_Phone.svg?react'
import PieChartSVG from '../assets/svg/Business_PieChart.svg?react'
import PresentationSVG from '../assets/svg/Business_Presentation.svg?react'
import ReportSVG from '../assets/svg/Business_Report.svg?react'
import SafeSVG from '../assets/svg/Business_Safe.svg?react'
import TargetSVG from '../assets/svg/Business_Target.svg?react'
import WorldSVG from '../assets/svg/Business_World.svg?react'
import DesktopMacSVG from '../assets/svg/Electronics_DesktopMac.svg?react'
import DesktopWindowsSVG from '../assets/svg/Electronics_DesktopWindows.svg?react'
import HeadphoneSVG from '../assets/svg/Electronics_Headphone.svg?react'
import KeyboardSVG from '../assets/svg/Electronics_Keyboard.svg?react'
import LaptopMacSVG from '../assets/svg/Electronics_LaptopMac.svg?react'
import LaptopWindowsSVG from '../assets/svg/Electronics_LaptopWindows.svg?react'
import MicrophoneSVG from '../assets/svg/Electronics_Microphone.svg?react'
import MouseSVG from '../assets/svg/Electronics_Mouse.svg?react'
import PhoneAndroidSVG from '../assets/svg/Electronics_PhoneAndroid.svg?react'
import PhoneIphoneSVG from '../assets/svg/Electronics_PhoneIphone.svg?react'
import PhotoSVG from '../assets/svg/Electronics_Photo.svg?react'
import PrinterSVG from '../assets/svg/Electronics_Printer.svg?react'
import RouterSVG from '../assets/svg/Electronics_Router.svg?react'
import ScannerSVG from '../assets/svg/Electronics_Scanner.svg?react'
import TabletMacSVG from '../assets/svg/Electronics_TabletMac.svg?react'
import TabletWindowsSVG from '../assets/svg/Electronics_TabletWindows.svg?react'
import TVSVG from '../assets/svg/Electronics_TV.svg?react'
import VideoSVG from '../assets/svg/Electronics_Video.svg?react'
import VolumeSVG from '../assets/svg/Electronics_Volume.svg?react'
import AssistanceSVG from '../assets/svg/Security_Assistance.svg?react'
import BugSVG from '../assets/svg/Security_Bug.svg?react'
import FingerprintSVG from '../assets/svg/Security_Fingerprint.svg?react'
import KeySVG from '../assets/svg/Security_Key.svg?react'
import LockSVG from '../assets/svg/Security_Lock.svg?react'
import PersonalIDSVG from '../assets/svg/Security_PersonalID.svg?react'
import SecuritySVG from '../assets/svg/Security_Security.svg?react'
import StopSVG from '../assets/svg/Security_Stop.svg?react'
import UnlockSVG from '../assets/svg/Security_Unlock.svg?react'
import VisibilitySVG from '../assets/svg/Security_Visibility.svg?react'
import VisibilityOffSVG from '../assets/svg/Security_VisibilityOff.svg?react'
import WarningSVG from '../assets/svg/Security_Warning.svg?react'
import WarningCircled1SVG from '../assets/svg/Security_WarningCircled1.svg?react'
import WarningCircled2SVG from '../assets/svg/Security_WarningCircled2.svg?react'
import BarcodeSVG from '../assets/svg/Shopping_Barcode.svg?react'
import BoxSVG from '../assets/svg/Shopping_Box.svg?react'
import CashVoucherSVG from '../assets/svg/Shopping_CashVoucher.svg?react'
import CouponSVG from '../assets/svg/Shopping_Coupon.svg?react'
import DeliverySVG from '../assets/svg/Shopping_Delivery.svg?react'
import FavoritesSVG from '../assets/svg/Shopping_Favorites.svg?react'
import GiftSVG from '../assets/svg/Shopping_Gift.svg?react'
import New2SVG from '../assets/svg/Shopping_New.svg?react'
import PackageSVG from '../assets/svg/Shopping_Package.svg?react'
import PercentSVG from '../assets/svg/Shopping_Percent.svg?react'
import PromotionSVG from '../assets/svg/Shopping_Promotion.svg?react'
import SalesSVG from '../assets/svg/Shopping_Sales.svg?react'
import ShoppingBasketSVG from '../assets/svg/Shopping_ShoppingBasket.svg?react'
import ShoppingCartSVG from '../assets/svg/Shopping_ShoppingCart.svg?react'
import StoreSVG from '../assets/svg/Shopping_Store.svg?react'
import WalletSVG from '../assets/svg/Shopping_Wallet.svg?react'
import AnchorSVG from '../assets/svg/Travel_Anchor.svg?react'
import BarSVG from '../assets/svg/Travel_Bar.svg?react'
import BeachSVG from '../assets/svg/Travel_Beach.svg?react'
import BusSVG from '../assets/svg/Travel_Bus.svg?react'
import CafeSVG from '../assets/svg/Travel_Cafe.svg?react'
import CampingSVG from '../assets/svg/Travel_Camping.svg?react'
import CarSVG from '../assets/svg/Travel_Car.svg?react'
import CurrencyExchangeSVG from '../assets/svg/Travel_CurrencyExchange.svg?react'
import DoorHangerSVG from '../assets/svg/Travel_DoorHanger.svg?react'
import ForestSVG from '../assets/svg/Travel_Forest.svg?react'
import HandwheelSVG from '../assets/svg/Travel_Handwheel.svg?react'
import HotelSVG from '../assets/svg/Travel_Hotel.svg?react'
import MapSVG from '../assets/svg/Travel_Map.svg?react'
import MapPointerSVG from '../assets/svg/Travel_MapPointer.svg?react'
import MountainsSVG from '../assets/svg/Travel_Mountains.svg?react'
import PassportSVG from '../assets/svg/Travel_Passport.svg?react'
import PlaneSVG from '../assets/svg/Travel_Plane.svg?react'
import PubSVG from '../assets/svg/Travel_Pub.svg?react'
import ReceptionBellSVG from '../assets/svg/Travel_ReceptionBell.svg?react'
import RestSVG from '../assets/svg/Travel_Rest.svg?react'
import RestaurantSVG from '../assets/svg/Travel_Restaurant.svg?react'
import ShipSVG from '../assets/svg/Travel_Ship.svg?react'
import SuitcaseSVG from '../assets/svg/Travel_Suitcase.svg?react'
import SwimSVG from '../assets/svg/Travel_Swim.svg?react'
import TaxiSVG from '../assets/svg/Travel_Taxi.svg?react'
import TrainSVG from '../assets/svg/Travel_Train.svg?react'
import WalkSVG from '../assets/svg/Travel_Walk.svg?react'
import CloudySVG from '../assets/svg/Weather_Cloudy.svg?react'
import DegreeCelsiusSVG from '../assets/svg/Weather_DegreeCelsius.svg?react'
import DegreeFahrenheitSVG from '../assets/svg/Weather_DegreeFahrenheit.svg?react'
import FogSVG from '../assets/svg/Weather_Fog.svg?react'
import HailSVG from '../assets/svg/Weather_Hail.svg?react'
import HumiditySVG from '../assets/svg/Weather_Humidity.svg?react'
import LightningSVG from '../assets/svg/Weather_Lightning.svg?react'
import MoonSVG from '../assets/svg/Weather_Moon.svg?react'
import PartlyCloudyDaySVG from '../assets/svg/Weather_PartlyCloudyDay.svg?react'
import PartlyCloudyNightSVG from '../assets/svg/Weather_PartlyCloudyNight.svg?react'
import RainSVG from '../assets/svg/Weather_Rain.svg?react'
import RainAndHailSVG from '../assets/svg/Weather_RainAndHail.svg?react'
import RainHeavySVG from '../assets/svg/Weather_RainHeavy.svg?react'
import RainLightSVG from '../assets/svg/Weather_RainLight.svg?react'
import SnowSVG from '../assets/svg/Weather_Snow.svg?react'
import SnowfallSVG from '../assets/svg/Weather_Snowfall.svg?react'
import SnowfallHeavySVG from '../assets/svg/Weather_SnowfallHeavy.svg?react'
import SnowfallLightSVG from '../assets/svg/Weather_SnowfallLight.svg?react'
import StormSVG from '../assets/svg/Weather_Storm.svg?react'
import SunnySVG from '../assets/svg/Weather_Sunny.svg?react'
import TemperatureSVG from '../assets/svg/Weather_Temperature.svg?react'
import UmbrellaSVG from '../assets/svg/Weather_Umbrella.svg?react'
import WaterSVG from '../assets/svg/Weather_Water.svg?react'
import WindSVG from '../assets/svg/Weather_Wind.svg?react'
import WindDirectionSVG from '../assets/svg/Weather_WindDirection.svg?react'

interface IconBaseProps extends React.HTMLProps<HTMLSpanElement> {
  spin?: boolean;
  rotate?: number;
}
interface CustomIconComponentProps {
  width: string | number;
  height: string | number;
  fill: string;
  viewBox?: string;
  className?: string;
  style?: React.CSSProperties;
}
interface IconComponentProps extends IconBaseProps {
  viewBox?: string;
  component?: React.ComponentType<CustomIconComponentProps | React.SVGProps<SVGSVGElement>> | React.ForwardRefExoticComponent<CustomIconComponentProps>;
  ariaLabel?: React.AriaAttributes['aria-label'];
}

export * from '@ant-design/icons'
export type AITIconType = (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => JSX.Element
export type IconName = "Add"|"AddCircled"|"Arrow1Down"|"Arrow1Left"|"Arrow1LeftDown"|"Arrow1LeftUp"|"Arrow1Right"|"Arrow1RightDown"|"Arrow1RightUp"|"Arrow1Up"|"Arrow2Down"|"Arrow2Left"|"Arrow2LeftDown"|"Arrow2LeftUp"|"Arrow2Right"|"Arrow2RightDown"|"Arrow2RightUp"|"Arrow2Up"|"Arrow3Down"|"Arrow3Left"|"Arrow3Right"|"Arrow3Up"|"Arrow4Down"|"Arrow4Left"|"Arrow4LeftDown"|"Arrow4LeftUp"|"Arrow4Right"|"Arrow4RightDown"|"Arrow4RightUp"|"Arrow4Up"|"Arrow5DownLeft"|"Arrow5DownRight"|"Arrow5LeftDown"|"Arrow5LeftUp"|"Arrow5RightDown"|"Arrow5RightUp"|"Arrow5UpLeft"|"Arrow5UpRight"|"Attach"|"Bell"|"Book"|"Bookmark"|"Calendar"|"Check"|"CheckCircled"|"Clear"|"Clock"|"Comment"|"Cookie"|"Copy"|"Cut"|"Database"|"Delete"|"DeleteCircled"|"Edit"|"EnvelopeClose"|"EnvelopeOpen"|"Favorite"|"Filter"|"Find"|"Flag"|"FolderClose"|"FolderOpen"|"Forbid"|"FullScreen"|"FullScreenExit"|"Home"|"Hyperlink"|"Image"|"Info"|"Label"|"List"|"New"|"Next"|"Options"|"Previous"|"Print"|"Question"|"Rating"|"Redo"|"Refresh"|"Reload"|"Remove"|"RemoveCircled"|"Rollback"|"Send"|"Settings"|"Sum"|"Table"|"Trash"|"Undo"|"User"|"Window"|"Zoom"|"Bank"|"BarChart"|"Briefcase"|"Businessman"|"Businesswoman"|"Calculator"|"Cash"|"CreditCard"|"Diagram"|"Dollar"|"DollarCircled"|"DoughnutChart"|"Euro"|"EuroCircled"|"Idea"|"LinearChart"|"Money"|"Phone"|"PieChart"|"Presentation"|"Report"|"Safe"|"Target"|"World"|"DesktopMac"|"DesktopWindows"|"Headphone"|"Keyboard"|"LaptopMac"|"LaptopWindows"|"Microphone"|"Mouse"|"PhoneAndroid"|"PhoneIphone"|"Photo"|"Printer"|"Router"|"Scanner"|"TabletMac"|"TabletWindows"|"TV"|"Video"|"Volume"|"Assistance"|"Bug"|"Fingerprint"|"Key"|"Lock"|"PersonalID"|"Security"|"Stop"|"Unlock"|"Visibility"|"VisibilityOff"|"Warning"|"WarningCircled1"|"WarningCircled2"|"Barcode"|"Box"|"CashVoucher"|"Coupon"|"Delivery"|"Favorites"|"Gift"|"New2"|"Package"|"Percent"|"Promotion"|"Sales"|"ShoppingBasket"|"ShoppingCart"|"Store"|"Wallet"|"Anchor"|"Bar"|"Beach"|"Bus"|"Cafe"|"Camping"|"Car"|"CurrencyExchange"|"DoorHanger"|"Forest"|"Handwheel"|"Hotel"|"Map"|"MapPointer"|"Mountains"|"Passport"|"Plane"|"Pub"|"ReceptionBell"|"Rest"|"Restaurant"|"Ship"|"Suitcase"|"Swim"|"Taxi"|"Train"|"Walk"|"Cloudy"|"DegreeCelsius"|"DegreeFahrenheit"|"Fog"|"Hail"|"Humidity"|"Lightning"|"Moon"|"PartlyCloudyDay"|"PartlyCloudyNight"|"Rain"|"RainAndHail"|"RainHeavy"|"RainLight"|"Snow"|"Snowfall"|"SnowfallHeavy"|"SnowfallLight"|"Storm"|"Sunny"|"Temperature"|"Umbrella"|"Water"|"Wind"|"WindDirection"
export const AITIcon: { [key in IconName]: AITIconType } = {
	Add: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={AddSVG} {...props} />,
	AddCircled: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={AddCircledSVG} {...props} />,
	Arrow1Down: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow1DownSVG} {...props} />,
	Arrow1Left: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow1LeftSVG} {...props} />,
	Arrow1LeftDown: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow1LeftDownSVG} {...props} />,
	Arrow1LeftUp: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow1LeftUpSVG} {...props} />,
	Arrow1Right: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow1RightSVG} {...props} />,
	Arrow1RightDown: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow1RightDownSVG} {...props} />,
	Arrow1RightUp: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow1RightUpSVG} {...props} />,
	Arrow1Up: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow1UpSVG} {...props} />,
	Arrow2Down: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow2DownSVG} {...props} />,
	Arrow2Left: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow2LeftSVG} {...props} />,
	Arrow2LeftDown: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow2LeftDownSVG} {...props} />,
	Arrow2LeftUp: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow2LeftUpSVG} {...props} />,
	Arrow2Right: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow2RightSVG} {...props} />,
	Arrow2RightDown: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow2RightDownSVG} {...props} />,
	Arrow2RightUp: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow2RightUpSVG} {...props} />,
	Arrow2Up: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow2UpSVG} {...props} />,
	Arrow3Down: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow3DownSVG} {...props} />,
	Arrow3Left: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow3LeftSVG} {...props} />,
	Arrow3Right: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow3RightSVG} {...props} />,
	Arrow3Up: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow3UpSVG} {...props} />,
	Arrow4Down: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow4DownSVG} {...props} />,
	Arrow4Left: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow4LeftSVG} {...props} />,
	Arrow4LeftDown: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow4LeftDownSVG} {...props} />,
	Arrow4LeftUp: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow4LeftUpSVG} {...props} />,
	Arrow4Right: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow4RightSVG} {...props} />,
	Arrow4RightDown: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow4RightDownSVG} {...props} />,
	Arrow4RightUp: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow4RightUpSVG} {...props} />,
	Arrow4Up: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow4UpSVG} {...props} />,
	Arrow5DownLeft: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow5DownLeftSVG} {...props} />,
	Arrow5DownRight: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow5DownRightSVG} {...props} />,
	Arrow5LeftDown: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow5LeftDownSVG} {...props} />,
	Arrow5LeftUp: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow5LeftUpSVG} {...props} />,
	Arrow5RightDown: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow5RightDownSVG} {...props} />,
	Arrow5RightUp: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow5RightUpSVG} {...props} />,
	Arrow5UpLeft: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow5UpLeftSVG} {...props} />,
	Arrow5UpRight: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={Arrow5UpRightSVG} {...props} />,
	Attach: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={AttachSVG} {...props} />,
	Bell: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={BellSVG} {...props} />,
	Book: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={BookSVG} {...props} />,
	Bookmark: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={BookmarkSVG} {...props} />,
	Calendar: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={CalendarSVG} {...props} />,
	Check: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={CheckSVG} {...props} />,
	CheckCircled: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={CheckCircledSVG} {...props} />,
	Clear: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={ClearSVG} {...props} />,
	Clock: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={ClockSVG} {...props} />,
	Comment: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={CommentSVG} {...props} />,
	Cookie: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={CookieSVG} {...props} />,
	Copy: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={CopySVG} {...props} />,
	Cut: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={CutSVG} {...props} />,
	Database: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={DatabaseSVG} {...props} />,
	Delete: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={DeleteSVG} {...props} />,
	DeleteCircled: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={DeleteCircledSVG} {...props} />,
	Edit: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={EditSVG} {...props} />,
	EnvelopeClose: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={EnvelopeCloseSVG} {...props} />,
	EnvelopeOpen: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={EnvelopeOpenSVG} {...props} />,
	Favorite: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={FavoriteSVG} {...props} />,
	Filter: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={FilterSVG} {...props} />,
	Find: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={FindSVG} {...props} />,
	Flag: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={FlagSVG} {...props} />,
	FolderClose: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={FolderCloseSVG} {...props} />,
	FolderOpen: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={FolderOpenSVG} {...props} />,
	Forbid: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={ForbidSVG} {...props} />,
	FullScreen: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={FullScreenSVG} {...props} />,
	FullScreenExit: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={FullScreenExitSVG} {...props} />,
	Home: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={HomeSVG} {...props} />,
	Hyperlink: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={HyperlinkSVG} {...props} />,
	Image: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={ImageSVG} {...props} />,
	Info: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={InfoSVG} {...props} />,
	Label: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={LabelSVG} {...props} />,
	List: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={ListSVG} {...props} />,
	New: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={NewSVG} {...props} />,
	Next: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={NextSVG} {...props} />,
	Options: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={OptionsSVG} {...props} />,
	Previous: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={PreviousSVG} {...props} />,
	Print: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={PrintSVG} {...props} />,
	Question: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={QuestionSVG} {...props} />,
	Rating: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={RatingSVG} {...props} />,
	Redo: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={RedoSVG} {...props} />,
	Refresh: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={RefreshSVG} {...props} />,
	Reload: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={ReloadSVG} {...props} />,
	Remove: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={RemoveSVG} {...props} />,
	RemoveCircled: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={RemoveCircledSVG} {...props} />,
	Rollback: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={RollbackSVG} {...props} />,
	Send: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={SendSVG} {...props} />,
	Settings: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={SettingsSVG} {...props} />,
	Sum: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={SumSVG} {...props} />,
	Table: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={TableSVG} {...props} />,
	Trash: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={TrashSVG} {...props} />,
	Undo: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={UndoSVG} {...props} />,
	User: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={UserSVG} {...props} />,
	Window: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={WindowSVG} {...props} />,
	Zoom: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={ZoomSVG} {...props} />,
	Bank: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={BankSVG} {...props} />,
	BarChart: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={BarChartSVG} {...props} />,
	Briefcase: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={BriefcaseSVG} {...props} />,
	Businessman: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={BusinessmanSVG} {...props} />,
	Businesswoman: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={BusinesswomanSVG} {...props} />,
	Calculator: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={CalculatorSVG} {...props} />,
	Cash: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={CashSVG} {...props} />,
	CreditCard: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={CreditCardSVG} {...props} />,
	Diagram: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={DiagramSVG} {...props} />,
	Dollar: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={DollarSVG} {...props} />,
	DollarCircled: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={DollarCircledSVG} {...props} />,
	DoughnutChart: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={DoughnutChartSVG} {...props} />,
	Euro: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={EuroSVG} {...props} />,
	EuroCircled: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={EuroCircledSVG} {...props} />,
	Idea: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={IdeaSVG} {...props} />,
	LinearChart: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={LinearChartSVG} {...props} />,
	Money: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={MoneySVG} {...props} />,
	Phone: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={PhoneSVG} {...props} />,
	PieChart: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={PieChartSVG} {...props} />,
	Presentation: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={PresentationSVG} {...props} />,
	Report: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={ReportSVG} {...props} />,
	Safe: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={SafeSVG} {...props} />,
	Target: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={TargetSVG} {...props} />,
	World: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={WorldSVG} {...props} />,
	DesktopMac: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={DesktopMacSVG} {...props} />,
	DesktopWindows: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={DesktopWindowsSVG} {...props} />,
	Headphone: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={HeadphoneSVG} {...props} />,
	Keyboard: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={KeyboardSVG} {...props} />,
	LaptopMac: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={LaptopMacSVG} {...props} />,
	LaptopWindows: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={LaptopWindowsSVG} {...props} />,
	Microphone: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={MicrophoneSVG} {...props} />,
	Mouse: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={MouseSVG} {...props} />,
	PhoneAndroid: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={PhoneAndroidSVG} {...props} />,
	PhoneIphone: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={PhoneIphoneSVG} {...props} />,
	Photo: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={PhotoSVG} {...props} />,
	Printer: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={PrinterSVG} {...props} />,
	Router: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={RouterSVG} {...props} />,
	Scanner: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={ScannerSVG} {...props} />,
	TabletMac: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={TabletMacSVG} {...props} />,
	TabletWindows: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={TabletWindowsSVG} {...props} />,
	TV: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={TVSVG} {...props} />,
	Video: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={VideoSVG} {...props} />,
	Volume: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={VolumeSVG} {...props} />,
	Assistance: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={AssistanceSVG} {...props} />,
	Bug: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={BugSVG} {...props} />,
	Fingerprint: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={FingerprintSVG} {...props} />,
	Key: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={KeySVG} {...props} />,
	Lock: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={LockSVG} {...props} />,
	PersonalID: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={PersonalIDSVG} {...props} />,
	Security: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={SecuritySVG} {...props} />,
	Stop: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={StopSVG} {...props} />,
	Unlock: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={UnlockSVG} {...props} />,
	Visibility: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={VisibilitySVG} {...props} />,
	VisibilityOff: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={VisibilityOffSVG} {...props} />,
	Warning: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={WarningSVG} {...props} />,
	WarningCircled1: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={WarningCircled1SVG} {...props} />,
	WarningCircled2: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={WarningCircled2SVG} {...props} />,
	Barcode: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={BarcodeSVG} {...props} />,
	Box: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={BoxSVG} {...props} />,
	CashVoucher: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={CashVoucherSVG} {...props} />,
	Coupon: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={CouponSVG} {...props} />,
	Delivery: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={DeliverySVG} {...props} />,
	Favorites: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={FavoritesSVG} {...props} />,
	Gift: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={GiftSVG} {...props} />,
	New2: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={New2SVG} {...props} />,
	Package: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={PackageSVG} {...props} />,
	Percent: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={PercentSVG} {...props} />,
	Promotion: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={PromotionSVG} {...props} />,
	Sales: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={SalesSVG} {...props} />,
	ShoppingBasket: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={ShoppingBasketSVG} {...props} />,
	ShoppingCart: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={ShoppingCartSVG} {...props} />,
	Store: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={StoreSVG} {...props} />,
	Wallet: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={WalletSVG} {...props} />,
	Anchor: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={AnchorSVG} {...props} />,
	Bar: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={BarSVG} {...props} />,
	Beach: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={BeachSVG} {...props} />,
	Bus: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={BusSVG} {...props} />,
	Cafe: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={CafeSVG} {...props} />,
	Camping: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={CampingSVG} {...props} />,
	Car: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={CarSVG} {...props} />,
	CurrencyExchange: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={CurrencyExchangeSVG} {...props} />,
	DoorHanger: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={DoorHangerSVG} {...props} />,
	Forest: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={ForestSVG} {...props} />,
	Handwheel: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={HandwheelSVG} {...props} />,
	Hotel: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={HotelSVG} {...props} />,
	Map: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={MapSVG} {...props} />,
	MapPointer: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={MapPointerSVG} {...props} />,
	Mountains: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={MountainsSVG} {...props} />,
	Passport: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={PassportSVG} {...props} />,
	Plane: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={PlaneSVG} {...props} />,
	Pub: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={PubSVG} {...props} />,
	ReceptionBell: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={ReceptionBellSVG} {...props} />,
	Rest: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={RestSVG} {...props} />,
	Restaurant: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={RestaurantSVG} {...props} />,
	Ship: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={ShipSVG} {...props} />,
	Suitcase: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={SuitcaseSVG} {...props} />,
	Swim: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={SwimSVG} {...props} />,
	Taxi: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={TaxiSVG} {...props} />,
	Train: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={TrainSVG} {...props} />,
	Walk: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={WalkSVG} {...props} />,
	Cloudy: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={CloudySVG} {...props} />,
	DegreeCelsius: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={DegreeCelsiusSVG} {...props} />,
	DegreeFahrenheit: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={DegreeFahrenheitSVG} {...props} />,
	Fog: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={FogSVG} {...props} />,
	Hail: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={HailSVG} {...props} />,
	Humidity: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={HumiditySVG} {...props} />,
	Lightning: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={LightningSVG} {...props} />,
	Moon: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={MoonSVG} {...props} />,
	PartlyCloudyDay: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={PartlyCloudyDaySVG} {...props} />,
	PartlyCloudyNight: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={PartlyCloudyNightSVG} {...props} />,
	Rain: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={RainSVG} {...props} />,
	RainAndHail: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={RainAndHailSVG} {...props} />,
	RainHeavy: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={RainHeavySVG} {...props} />,
	RainLight: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={RainLightSVG} {...props} />,
	Snow: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={SnowSVG} {...props} />,
	Snowfall: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={SnowfallSVG} {...props} />,
	SnowfallHeavy: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={SnowfallHeavySVG} {...props} />,
	SnowfallLight: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={SnowfallLightSVG} {...props} />,
	Storm: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={StormSVG} {...props} />,
	Sunny: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={SunnySVG} {...props} />,
	Temperature: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={TemperatureSVG} {...props} />,
	Umbrella: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={UmbrellaSVG} {...props} />,
	Water: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={WaterSVG} {...props} />,
	Wind: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={WindSVG} {...props} />,
	WindDirection: (props: IconComponentProps & React.RefAttributes<HTMLSpanElement>) => <Icon component={WindDirectionSVG} {...props} />
}

export { Icon }